// Обьясните своими словами, как вы понимаете, как работает прототипное наследование в Javascript
// Есть основная инструкция на основе которой создают последующие похожие. Например есть основная конструкция машины с определенными свойствами/методами (скорость/цвет/встроенные функции и тд), на основе которой можно сделать машину такую же, но другого цвета, быстрее/медленней, с большим или меньшим спектром функций. Основная модель - это прототип для всех последующих, сделанных на ее основе машин.

"use strict";

class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }

  set name(value) {
    this._name = value;
  }
  get name() {
    return this._name;
  }

  set age(value) {
    this._age = value;
  }
  get age() {
    return this._age;
  }

  set salary(value) {
    this._salary = value;
  }
  get salary() {
    return this._salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, langs) {
    super(name, age, salary);
    this.langs = langs;
  }
}
Object.defineProperty(Programmer.prototype, "salary", {
  get: function () {
    return this._salary;
  },
  set: function (value) {
    this._salary = value * 3;
  },
});

const programmer1 = new Programmer("Vasiliy", 25, 1000, [
  "english",
  "german",
  "russian",
]);
console.log(programmer1);

const programmer2 = new Programmer("Anna", 33, 5000, [
  "english",
  "ukrainian",
  "french",
  "chinese",
]);
console.log(programmer2);

const programmer3 = new Programmer("Mariia", 27, 10000, [
  "english",
  "german",
  "russian",
  "ukrainian",
  "spanish",
]);
console.log(programmer3);
