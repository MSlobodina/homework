"use strict";

//Обьясните своими словами, как вы понимаете, что такое деструктуризация и зачем она нужна
// С помощью деструктуризации из обьекта или массива (особенно с большим количеством данных) можно просто "вытянуть" нужный фрагмент (значения, функции тд) и записать в переменную с которой можно работать отдельно от целого обьекта/массива. Добавлять свойства, указывать значения по умолчанию, выбирать нужные данные из обьекта/массива, формируя с ними новую структуру.

// ### Задание 1

// const clients1 = [
//   "Гилберт",
//   "Сальваторе",
//   "Пирс",
//   "Соммерс",
//   "Форбс",
//   "Донован",
//   "Беннет",
// ];
// const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];
// const clientBase = [...clients1, ...clients2];
// const uniqueClientBase = new Set(clientBase);
// console.log(uniqueClientBase);

// ### Задание 2

// const characters = [
//   {
//     name: "Елена",
//     lastName: "Гилберт",
//     age: 17,
//     gender: "woman",
//     status: "human",
//   },
//   {
//     name: "Кэролайн",
//     lastName: "Форбс",
//     age: 17,
//     gender: "woman",
//     status: "human",
//   },
//   {
//     name: "Аларик",
//     lastName: "Зальцман",
//     age: 31,
//     gender: "man",
//     status: "human",
//   },
//   {
//     name: "Дэймон",
//     lastName: "Сальваторе",
//     age: 156,
//     gender: "man",
//     status: "vampire",
//   },
//   {
//     name: "Ребекка",
//     lastName: "Майклсон",
//     age: 1089,
//     gender: "woman",
//     status: "vempire",
//   },
//   {
//     name: "Клаус",
//     lastName: "Майклсон",
//     age: 1093,
//     gender: "man",
//     status: "vampire",
//   },
// ];

// const charactersShortInfo = characters.map((person) => {
//   const { name, lastName, age } = person;
//   return {
//     name,
//     lastName,
//     age,
//   };
// });

// console.log(charactersShortInfo);

// ### Задание 3

// const user1 = {
//   name: "John",
//   years: 30,
// };

// let { name, years: age, isAdmin = false } = user1;
// console.log(name, age, isAdmin);

// const root = document.createElement("div");
// document.body.prepend(root);
// root.append(
//   `User1 name is ${name}, he is  ${age} years old. Is he admin? ${isAdmin}`
// );

// ### Задание 4

// const satoshi2020 = {
//   name: "Nick",
//   surname: "Sabo",
//   age: 51,
//   country: "Japan",
//   birth: "1979-08-21",
//   location: {
//     lat: 38.869422,
//     lng: 139.876632,
//   },
// };

// const satoshi2019 = {
//   name: "Dorian",
//   surname: "Nakamoto",
//   age: 44,
//   hidden: true,
//   country: "USA",
//   wallet: "1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa",
//   browser: "Chrome",
// };

// const satoshi2018 = {
//   name: "Satoshi",
//   surname: "Nakamoto",
//   technology: "Bitcoin",
//   country: "Japan",
//   browser: "Tor",
//   birth: "1975-04-05",
// };

// function makeFullProfile(...data) {
//   const fullProfile = Object.assign({}, ...data);
//   return fullProfile;
// }
// const satoshiFull = makeFullProfile(satoshi2018, satoshi2019, satoshi2020);
// console.log(satoshiFull);

// ### Задание 5

// const books = [
//   {
//     name: "Harry Potter",
//     author: "J.K. Rowling",
//   },
//   {
//     name: "Lord of the rings",
//     author: "J.R.R. Tolkien",
//   },
//   {
//     name: "The witcher",
//     author: "Andrzej Sapkowski",
//   },
// ];

// const bookToAdd = {
//   name: "Game of thrones",
//   author: "George R. R. Martin",
// };

// var1;
// const allBooks = books.slice();
// allBooks.push(bookToAdd);
// console.log(allBooks);
// console.log(books);

//var2;
// const [...allBooks] = books;
// allBooks.push(bookToAdd);
// console.log(allBooks);
// console.log(books);

// ### Задание 6

// const employee = {
//   name: "Vitalii",
//   surname: "Klichko",
// };

// const newEmployee = { ...employee };

// newEmployee.age = 5;
// newEmployee.salary = 20 + "$";

// console.log(newEmployee);
// console.log(employee);

// Задание 7
// Дополните код так, чтоб он был рабочим
// alert(value); // должно быть выведено 'value'
// alert(showValue());  // должно быть выведено 'showValue'

// я не понимаю что тут нужно добавить

// const array = ["value", () => "showValue"];

// let [value, showValue] = array;
// alert(value);
// alert(showValue());
