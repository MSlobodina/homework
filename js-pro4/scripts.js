"use strict";
//Обьясните своими словами, что такое AJAX и чем он полезен при разработке на Javascript.
// AJAX - Асинхронный JavaScript и XML. Это набор методов веб-разработки, которые позволяют веб-приложениям работать асинхронно — обрабатывать любые запросы к серверу в фоновом режиме. Такая технология позволяет перезагружать определенный элемент страницы вместо обновления всей страницы, при этом сообщать пользователю что происходит путем индикаторов загрузки или текстовых сообщений (например онлайн чат интернет магазина, заполнение формы, автозаполнение в Google search)

const URL = "https://ajax.test-danit.com/api/swapi/";
const root = document.getElementById("root");

class Films {
  constructor() {}
  requestFilms() {
    axios
      .get("https://ajax.test-danit.com/api/swapi/films")
      .then(function (response) {
        //console.log(response.data);
        const data = response.data;

        data.sort((a, b) => {
          return b.episodeId - a.episodeId;
        });

        data.forEach((el) => {
          const p = document.createElement("p");
          p.textContent = `Episode № ${el.episodeId} - Film: "${el.name}" : "${el.openingCrawl}"`;
          root.prepend(p);

          const ol = document.createElement("ol");
          p.append(ol);

          let charactersList = el.characters;
          charactersList.forEach((item) => {
            let listItems = document.createElement("li");
            ol.append(listItems);
            listItems.innerHTML += item;
          });
          console.log(el);
        });
      });
  }
}
const films = new Films();
films.requestFilms();
