"use strict";
//Приведите пару примеров, когда уместно использовать в коде конструкцию `try...catch`. Когда какой-то блок кода может вызвать ошибку, чтобы выполнение всей программы не прервалось используется try/catch. В таком случае catch ловит ошибку в определенном блоке кода и сигнализирует об этом, не останавливая выполнение всей программы. Или когда нужно чтобы определенная функция завершилась, не зависимо от того произошла ли ошибка.

const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70,
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40,
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  },
];

const root = document.createElement("div");
root.setAttribute("id", this.id);
root.id = "root";
document.body.prepend(root);

const list = document.createElement("ul");
root.append(list);

books.forEach((item, index) => {
  try {
    if (!item.hasOwnProperty("author")) {
      throw new Error(`Object #${index + 1} has no author name`);
    } else if (!item.hasOwnProperty("name")) {
      throw new Error(`Object #${index + 1} has no book name`);
    } else if (!item.hasOwnProperty("price")) {
      throw new Error(`Object #${index + 1} has no price`);
    }
  } catch (e) {
    console.log(e.message);
  } finally {
    if (
      item.hasOwnProperty("author") &&
      item.hasOwnProperty("name") &&
      item.hasOwnProperty("price")
    )
      list.innerHTML += `<li>${item.author} - "${item.name}", ${item.price} грн.</li>`;
  }
});
